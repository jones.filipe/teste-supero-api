package com.supero.teste.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.supero.teste.api.model.Task;

public interface TaskRepository extends JpaRepository<Task, Long> {


}
