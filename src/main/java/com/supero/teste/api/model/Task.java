package com.supero.teste.api.model;

import static java.lang.Boolean.FALSE;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table
public class Task {
	
	@Id
	@GeneratedValue
	public Long id;
	public String titulo;
	public String descricao;
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	public Date dataCriacao;
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	public Date dataAtualizacao;
	public Boolean concluido = FALSE;
		
	public void atualizarStatus() {
		setConcluido(!getConcluido());
	}

}
