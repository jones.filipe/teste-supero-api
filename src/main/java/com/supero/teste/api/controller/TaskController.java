package com.supero.teste.api.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.supero.teste.api.model.Task;
import com.supero.teste.api.repository.TaskRepository;

@RestController
@RequestMapping("/task")
public class TaskController {
	
	@Autowired
	private TaskRepository repository;
	
	@PostMapping
	public Task salvar(@RequestBody Task task) {
		return repository.save(task);
	}
	
	@GetMapping
	public List<Task> findTasks() {
		return repository.findAll();
	}
	
	@GetMapping("/{id}")
	public Task getTask(@PathVariable Long id) {
		return repository.findById(id).orElse(null);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		repository.deleteById(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@GetMapping("/atualizar-status/{id}")
	public Task atualizarStatus(@PathVariable Long id) {
		Optional<Task> task = repository.findById(id);
		if(task.isPresent()) {
			task.get().atualizarStatus();
			repository.save(task.get());
		}
		return repository.findById(id).orElse(null);
	}
}
